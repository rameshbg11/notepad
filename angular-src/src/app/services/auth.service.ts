import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
 import { HttpModule } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class AuthService {

  constructor(private http:HttpClient) { }

  authenticateUser(user){
    let headers= new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/authenticate',user);

  }

}
