import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth:AuthService) { }

  ngOnInit() {
    this.loginUser();
  }

  loginUser(){
    var user={
      "username":"ram",
      "password":"12345"
    }
    //Auth with user credentials
    this.auth.authenticateUser(user).subscribe( data =>
    console.log(data));
  }

}
